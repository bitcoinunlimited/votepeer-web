import { IAuth } from './model/auth';

// eslint-disable-next-line import/prefer-default-export
export function mockAuth(): IAuth {
  const login: IAuth = {
    challenge: 'challengestring',
    address: 'addressstring',
    signature: 'signaturestring',
  };

  return login;
}
