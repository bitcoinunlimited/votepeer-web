const functions = require('firebase-functions');

export function getRegion() {
  return functions.config().votepeer.firebase_region;
}

export function getDbURL() {
  return functions.config().votepeer.firebase_database_url;
}

export function getProjectId() {
  return functions.config().votepeer.firebase_project_id;
}

export function getServiceAccountId() {
  return functions.config().votepeer.firebase_service_account_id;
}

export function getStorageBucket() {
  return functions.config().votepeer.firebase_storage_bucket;
}

export function firebaseProjectCfg() {
  return {
    databaseURL: getDbURL(),
    projectId: getProjectId(),
    serviceAccountId: getServiceAccountId(),
    storageBucket: getStorageBucket(),
  };
}
