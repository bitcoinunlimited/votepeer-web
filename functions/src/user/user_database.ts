import 'firebase/app';
import admin from 'firebase-admin';
import { firebaseAdminFirestore } from '../db';
import { constants } from '../constants';
import { DoesNotExistError } from '../error';

async function getAndroidNotificationToken(
  userId: string,
): Promise<string | null> {
  const userSnap = await firebaseAdminFirestore().collection(constants.user.path).doc(userId)
    .get()
    .catch((e: Error) => {
      console.warn('user', 'Get election user: %s', e);
      return Promise.reject(e.toString());
    });
  const androidNotificationToken = userSnap.get('androidNotificationToken');
  if (androidNotificationToken === undefined) {
    Promise.resolve(null);
  }
  return Promise.resolve(androidNotificationToken);
}

export async function getAndroidNotificationTokens(
  participantAddresses: string[],
): Promise<Array<string |null>> {
  const tokenPromises: Array<Promise<string | null>> = [];
  for (const address of participantAddresses) {
    const androidNotificationTokenPromise = getAndroidNotificationToken(address);
    tokenPromises.push(androidNotificationTokenPromise);
  }

  return Promise.all(tokenPromises);
}

export async function getUser(userID: string): Promise<any> {
  const user = await firebaseAdminFirestore().collection(constants.user.path)
    .doc(userID)
    .get();
  if (user === undefined) {
    throw new DoesNotExistError(`User ${userID} does not exist`);
  }
  return user;
}

export async function getRingSignaturePubkey(userID: string): Promise<string> {
  const u = constants.user;
  const userSnap = await firebaseAdminFirestore().collection(u.path).doc(userID).get();
  return userSnap.get(u.fields.link_ringsig_pubkey);
}

export async function hasRingSignaturePubKey(userID: string): Promise<boolean> {
  return await getRingSignaturePubkey(userID) !== undefined;
}

/**
 * Store proof that user owns public key for use in ring signature voting.
 */
export async function storeRingsigLink(
  userID: string, pubkey: string,
  message: string, proof: string,
): Promise<any> {
  return firebaseAdminFirestore().collection(constants.user.path)
    .doc(userID)
    .set({
      link_ringsig_pubkey: pubkey,
      link_ringsig_proof: proof,
      link_ringsig_message: message,
    }, { merge: true });
}

/**
 * Caution: Users should not be able to delete or replace their link.
 * This function exists for unit testing.
 */
export async function deleteRingSigLink(userID: string): Promise<any> {
  try {
    return await firebaseAdminFirestore().collection(constants.user.path)
      .doc(userID)
      .update({
        link_ringsig_pubkey: admin.firestore.FieldValue.delete(),
        link_ringsig_proof: admin.firestore.FieldValue.delete(),
        link_ringsig_message: admin.firestore.FieldValue.delete(),
      });
  } catch (e: any) {
    if (e.code === 5 /* field does not exist */) {
      throw new DoesNotExistError(
        `Ring signature link does not exist for user ${userID}`,
      );
    }
    throw e;
  }
}

async function createArrayIfMissing(
  ref: admin.firestore.DocumentReference,
  field: string,
): Promise<void> {
  const doc = await ref.get();

  if (doc.get(field) !== undefined) {
    // Field exists
    return;
  }

  // Initialize to empty array.
  await ref.set({
    [field]: [],
  });
}

/**
 * Add tags a user is allowed to add to an election.
 */
export async function addUserAllowedTags(userID: string, tags: string[]): Promise<any> {
  const userRef = await firebaseAdminFirestore()
    .collection(constants.user.path)
    .doc(userID);

  await createArrayIfMissing(userRef, constants.user.fields.allowed_tags);

  return Promise.all(tags.map((t) => userRef.update({
    [constants.user.fields.allowed_tags]: admin.firestore.FieldValue.arrayUnion(t),
  })));
}

/**
 * Fetch list of tags that user is allowed to assign to an election.
 */
export async function getUserAllowedTags(userID: string): Promise<string[]> {
  const userSnap = await firebaseAdminFirestore()
    .collection(constants.user.path)
    .doc(userID)
    .get();
  const allowedTags = await userSnap.get(constants.user.fields.allowed_tags);
  return allowedTags === undefined ? [] : allowedTags;
}

/**
 * Delete tags user is allowed to assign to an election.
 */
export async function deleteUserAllowedTags(
  userID: string,
  tags: string[],
): Promise<any> {
  const userRef = await firebaseAdminFirestore()
    .collection(constants.user.path)
    .doc(userID);

  await createArrayIfMissing(userRef, constants.user.fields.allowed_tags);

  return Promise.all(tags.map((t) => userRef.update({
    [constants.user.fields.allowed_tags]: admin.firestore.FieldValue.arrayRemove(t),
  })));
}

export function deleteUser(userID: string): Promise<any> {
  return firebaseAdminFirestore()
    .collection(constants.user.path)
    .doc(userID)
    .delete();
}
